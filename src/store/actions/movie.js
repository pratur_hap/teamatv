import { GET_MOVIES, ERROR, GET_GENRES } from "./types";
const baseUrl = "http://notflixtv.herokuapp.com/api/v1";

// GET Data Movies
export const getMovies = (page = 1) => async dispatch => {
  try {
    const res = await fetch(`${baseUrl}/movies?page=${page}`);
    const data = await res.json();
    dispatch({
      type: GET_MOVIES,
      payload: data.data
    });
  } catch (error) {
    console.log(error);
    dispatch({
      type: ERROR,
      payload: error
    });
  }
};

// GET GENRES
export const getGenres = () => async dispatch => {
  try {
    const res = await fetch(`${baseUrl}/movies/genres`);
    const data = await res.json();
    dispatch({
      type: GET_GENRES,
      payload: data.data
    });
  } catch (error) {
    console.log(error);
    dispatch({
      type: ERROR,
      payload: error
    });
  }
};





// // ADD Data Movie
// export const addMovie = post => async dispatch => {
//   try {
//     const res = await fetch(`${baseUrl}/todos`, {
//       method: "POST",
//       headers: {
//         "Content-Type": "application/json"
//       },
//       body: JSON.stringify(post)
//     });
//     const data = await res.json();
//     dispatch({
//       type: ADD_POST,
//       payload: data
//     });
//   } catch (err) {
//     console.log(err);
//   }
// };

// // DELETE Data Movie
// export const deleteMovie = id => async dispatch =>{
//     try {
//         const res = await fetch(`${baseUrl}/todos/${id}`,{
//         method: DELETE
//     })
//     await res.json()
//     dispatch({
//         type: DELETE,
//         payload:id
//     })
//     } catch (err){
//         console.log(err)
//     }
// }

