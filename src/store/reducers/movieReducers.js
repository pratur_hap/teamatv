import {
  GET_MOVIES,
  GET_GENRES,
  ERROR,
  ADD_POST,
  DELETE
} from "../actions/types";
const initialState = {
  movies: [],
  genres: [],
  pageInfo:[],
  errors: null
};

const movieReducers = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_MOVIES:
      const {
        docs,
        limit,
        totalPages,
        page,
        pagingCounter,
        hasPrevPage,
        hasNextPage,
        prevPage,
        nextPage
      } = payload;
      return {
        ...state,
        movies: docs,
        pageInfo: {
          limit,
          totalPages,
          page,
          pagingCounter,
          hasPrevPage,
          hasNextPage,
          prevPage,
          nextPage
        }
      };
    case ERROR:
      return {
        ...state,
        errors: payload
      };
    case GET_GENRES:
      return {
        ...state,
        genres: payload
      };
    // case ADD_POST:
    //   return {
    //     ...state,
    //     movies: [...state.movies, payload]
    //   };
    // case DELETE :
    //     return {
    //         ...state,
    //         movies : state.movies.filter(item => item.id !== payload)
    //     }

    default:
      return {
        ...state
      };
  }
};

export default movieReducers;
