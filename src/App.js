import React, { useSate } from "react";
import { connect } from "react-redux";
import { getMovies, addMovie, deleteMovie } from "./store/actions/movie.js";


const App = ({ movies, getMovies, addMovie, deleteMovie }) => {
  const [title, setTitle] = React.useState("");

  React.useEffect(() => {
    getMovies();
  }, [getMovies]);

  const lists = movies.map(item => 
  <li key={item.id}>{item.title}<button onClick={()=> deleteMovie(item.id)}>delete</button></li>);

  const change = e => {
    setTitle(e.target.value);
  };

  const submit = e => {
    e.preventDefault()
    const post = {
      title
    };
    addMovie(post);
    setTitle("");
  };

  

  return (
    <div className="App">
      <h1>TeamAtv</h1>
      <form onSubmit={submit}>
        <input
          type="text"
          placeholder="title"
          style={{ width: "50%" }}
          value={title}
          onChange={change}
        ></input>
        <button>add</button>
      </form>
      <ol>{lists}</ol>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    movies: state.movieReducers.movies
  };
};

export default connect(mapStateToProps, { getMovies, addMovie, deleteMovie })(App);
