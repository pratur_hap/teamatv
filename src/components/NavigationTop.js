import React from "react";
// import { NavLink } from "react-router-dom";

function NavigationTop() {
  return (
    // <nav>
    //   <NavLink exact activeClassName="active" to="/">
    //     Home
    //   </NavLink>
    //   <NavLink activeClassName="active" to="/users">
    //     Users
    //   </NavLink>
    //   <NavLink activeClassName="active" to="/contact">
    //     Contact
    //   </NavLink>
    // </nav>
    <div className="bg-nav">
      <div className="container">
        <div className="logo">
          <i class="fas fa-comments fa-2x"></i>&nbsp;&nbsp;<b>TeamA-TV</b>
        </div>
        <form>
          <i class="fas fa-search"></i>
          <input
            type="search"
            name="search"
            className="search"
            placeholder="search movie..."
          ></input>
        </form>
        <div className="button-sign">
          <a href=".">SIGN IN</a>
        </div>
      </div>
    </div>
  );
}
export default NavigationTop;
