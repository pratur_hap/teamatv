import React from "react";
import Slider from "react-slick";

class SliderComponents extends React.Component {
  render() {
    var settings = {
      autoplay:true,
      autoplaySpeed:3000,
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      draggable:false
    };
    return (
      <Slider {...settings} >
        <div>
            <img src="https://www.cgteam.com/wp-content/uploads/2019/01/CG-Homepage-Header-Taxes-1366x450.png" alt=""></img> 
        </div>
        <div>
        <img src="https://www.cgteam.com/wp-content/uploads/2019/01/CG-Homepage-Header-Taxes-1366x450.png" alt=""></img>
        </div>
        <div>
        <img src="https://www.cgteam.com/wp-content/uploads/2019/01/CG-Homepage-Header-Taxes-1366x450.png" alt=""></img>
        </div>
        
      </Slider>
    );
  }
}
export default SliderComponents;