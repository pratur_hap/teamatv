import React, { Component } from "react";
import { connect } from "react-redux";
import { getMovies, getGenres } from "../../store/actions/movie";

class ContentHome extends Component {
  componentDidMount() {
    this.props.getMovies();
    this.props.getGenres();
  }


  handlePages(pagelist){
    this.props.getMovies(pagelist);
  }

  handleCategory(moviecategory){
    this.props.getMovies(moviecategory);
  }

  totalPages= () => {
    const pages = []
    for(let i = 1; i < this.props.pageInfo.totalPages + 1; i++){
        pages.push(i)
    }
    return pages.map((item) => (
        <p className="number-page" onClick={(e) =>this.handlePages(item)}>{item}</p>
    ))
  }

  render() {
    return (
      <React.Fragment>
        <div className="content">
          <div>
            <h3 className="title">Browse by category</h3>
          </div>
          <div className="list-category">
            {this.props.genres.map(genre => (
              <button value={genre} onClick={(e)=>this.handleCategory(genre)}>
                {genre}
              </button>
            ))}
          </div>

          <div className="list-movie">
            {this.props.movies.map(item => (
              <div className="list">
                <img
                  src={`https://image.tmdb.org/t/p/original/${item.poster}`}
                  className="image-movie"
                  alt={`pict of ${item.title}`}
                ></img>
                <p className="title-movie">{item.title}</p>
                <p className="category-movie">{item.genres[0]}</p>
              </div>
            ))}
          </div>
        </div>

        <div className="pagination">
          <i className="fas fa-arrow-left fa-2x"></i>
          
          {this.totalPages()}

          <i className="fas fa-arrow-right fa-2x"></i>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    movies: state.movieReducers.movies,
    pageInfo: state.movieReducers.pageInfo,
    genres: state.movieReducers.genres
  };
};

export default connect(mapStateToProps, { getMovies, getGenres })(ContentHome);
