import React, { Component } from 'react'
import NavigationTop from "../NavigationTop";
import FooterBot from "../FooterBot";
import ContentHome from "./ContentHome";
import SliderComponents from "./SliderComponents";
import PageContent from "./PageContent";


class HomePage extends Component {
    render() {
        return (
            <div className="layout">
                <NavigationTop/>
                <SliderComponents/>
                <ContentHome/>
                <PageContent/>
                <FooterBot/>
            </div>
        )
    }
}

export default HomePage;