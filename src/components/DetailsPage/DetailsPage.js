import React, { Component } from 'react';
import NavigationTop from "../NavigationTop";
import FooterBot from "../FooterBot";
import HeaderImage from "./HeaderImages";
import SubMenu from "./SubMenu";
import Overview from "./Overview";

class DetailsPage extends Component {
    render() {
        return (
            <div className="layout">
                <NavigationTop/>
                {/* <HeaderImage/> */}
                <SubMenu/>
                <Overview/>
                <FooterBot/>
            </div>
        )
    }
}
export default DetailsPage;
