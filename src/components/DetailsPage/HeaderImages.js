import React, { Component } from 'react'

class HeaderImages extends Component {
    render() {
        return (
            <div className="bg-image">
                <div className="img-bg">
                    <img src="https://www.abp-ip.com/wp-content/uploads/2017/09/red-header-03.png" alt=""></img>
                    
                </div>
                <div className="container">
                    <h3>SEINT SEIYA</h3>
                    
                </div>
                
            </div>
        )
    }
}

export default HeaderImages;
