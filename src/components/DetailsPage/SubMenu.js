import React, { Component } from 'react'

export default class SubMenu extends Component {
    render() {
        return (
            <div className="content">
                <div className="list-category">
                    <button value="all">Overview</button>
                    <button value="anime">Character</button>
                    <button value="action">Review</button>
                </div>
            </div>
        )
    }
}
