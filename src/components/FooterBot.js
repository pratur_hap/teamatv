import React from "react";

function FooterBot() {
  return (
    <div className="bg-footer">
      <div className="container-footer">
        <div className="content">
          <div className="about">
            <div className="logo">
              <i class="fas fa-comments fa-3x"></i>
              <h4>Team A TV</h4>
            </div>

            <p>
              TeamA-TV adalah sebuah website untuk mereview film. Website ini
              dibuat oleh Team A yang terdiri dari kelompok backend, frontend
              dan juga mobile. Afdallah - Luki ; Pratur - Renaldi; Asep
            </p>
          </div>
          <div className="menu">
            <h3>Link</h3>
            <p>About us</p>
            <p>Blog</p>
            <p>Service</p>
            <p>Career</p>
            <p>Contact</p>
          </div>
          <div className="link">
            <div className="badge">
              <h3>Download</h3>
              <img
                src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png"
                width="50%"
                alt="googleplay"
              />
              <br></br>
              <img
                src="https://www.zionitconsulting.com/wp-content/uploads/2018/09/ios-badge.png"
                alt="ios"
                width="50%"
              ></img>
            </div>
            <div className="sosmed">
              <h3>Social Media</h3>
              <i class="fab fa-facebook-square fa-2x" title="facebook"></i>
              <i class="fab fa-pinterest-square fa-2x" title="pinterest"></i>
              <i class="fab fa-instagram-square fa-2x" title="instagram"></i>
            </div>
          </div>
        </div>
        <hr></hr>
        <div className="copyright">
          <p>Copyright© 2020 Team A TV. All Rights Reserved </p>
        </div>
      </div>
    </div>
  );
}

export default FooterBot;
